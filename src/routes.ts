import {Routes} from '@angular/router';
import {ChatComponent} from './app/chat/chat.component';
import {LoginComponent} from './app/login/login.component';
import {AuthGuard} from './app/auth.guard';

export const appRoutes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'chat', component: ChatComponent, canActivate: [AuthGuard]}
];
