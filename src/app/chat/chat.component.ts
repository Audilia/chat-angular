import { Component, OnInit } from '@angular/core';
import {MessageService} from '../message.service';
import {Router} from '@angular/router';
import {Message} from '../message';
import {AuthService} from '../auth.service';
import {User} from '../user';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  messages: Message[];
  users: User[];
  isAuthenticated = true;

  constructor(private messageService: MessageService,
              private router: Router,
              private authService: AuthService) { }

  ngOnInit() {
    console.log(this.messageService)
    this.messageService.messagesSubject.subscribe(
      (messages) => {
        console.log(messages)
        this.messages = messages;
      }
    )
    this.messageService.getMessages();
  }

}
