export interface User {
  id: string;
  pseudo: string;
  city: string;
  birth: Date;
}
