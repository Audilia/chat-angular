import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  messageForm: FormGroup;
  constructor(
    private messageService: MessageService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.initForm();
    }

  ngOnInit() {
  }

  initForm() {
    this.messageForm = this.formBuilder.group({
      content: ['', Validators.required],
      user: ['', Validators.required],
    });
  }

  onSubmit() {
    const message = this.messageForm.value.content;
    const user_id = this.messageForm.value.user;
    this.messageService.addMessage(message, user_id);
    this.router.navigate(['/chat']);
  }

  // onPrivate() {
  //   const message = this.messageForm.value;
  //   const id = this.messageForm.value;
  //   this.messageService.privateMessage(message, id);
  //   this.router.navigate(['/']);
  // }
}
