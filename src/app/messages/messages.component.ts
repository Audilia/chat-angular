import { Component, OnInit } from '@angular/core';
import {MessageService} from '../message.service';
import {Message} from '../message';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  message: Message;


  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService) { }

  ngOnInit() {

  }

}
