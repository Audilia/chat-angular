import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {map} from 'rxjs/operator/map';
import { transform } from './tools/helpers';
import {Message} from './message';
import {User} from './user';

@Injectable()
export class MessageService {

  public readonly API: string = `http://192.168.1.56/chat-symfony/public`;
  messages: Message[] = [];
  users: User[] = []
  public messagesSubject: Subject<Message[]> = new Subject<Message[]>();
  public userSubject: Subject<User[]> = new Subject <User[]>();

  constructor(private http: HttpClient) { }

  emitMessages() {
    this.messagesSubject.next(this.messages);
  }

  emitUsers() {
    this.userSubject.next(this.users);
  }


  // findById(id: string) {
  //   return this.messages.find((message) => message.id === id);
  // }

  getMessages() {
    this.http.get<any>(`${this.API}/messages/all`)
      .subscribe(
        (messages) => {
          this.messages = Object.keys(messages).map(function(key) {
            return messages[key];
          });
          this.emitMessages();
        },
        // error => { console.error('MessageService.getMessages(): ', error); },
        // () => { console.info('MessageService.getMessages(): complete'); }
      );
  }

  public addMessage(content, user_id) {

    const o = {
      channel: null,
      content,
      id: user_id,
      user: {
        pseudo : '',
      }
    }
    return this.http.post<{name}>(`${this.API}/messages/add`, {'content' : content, 'user_id' : user_id}).subscribe(
      data => {
        this.messages.push(o)
        this.emitMessages();
      },
      // error => console.error('AdService.addAd(): ', error),
      // () => console.info('AdService.addAd(): complete')
    );
  }

  // public privateMessage(message, id) {
  //   return this.http.post<{name}>(`${this.API}/private.json`, message, id).subscribe(
  //     data => {
  //       this.messages.push(message, id);
  //       this.emitMessages();
  //     },
  //   );
  // }

  public deleteMessage(id) {
    const request = this.http.delete<{name}>(`${this.API}/messages/delete/${id}`)
    request.subscribe(
      (data) => {
        const index = this.messages.findIndex(message => message.id === id);
        this.messages.splice(index, 1);
        this.emitMessages();
      }
    );
    return request;
  }

  // public insertOrUpdate(newMessage) {
  //   const index = this.messages.findIndex(ad => ad.id === newMessage.id); // cherche l'index de l'annonce dans le tableau ads
  //   if (index > -1) {
  //     this.messages[index] = newMessage; // remplacement de l'annonce existante
  //   }
  //   else {
  //     this.messages.push(newMessage); // ajout d'une nouvelle annonce
  //   }
  // }
  // public updateMessage(message: Message) {
  //   return this.http.put<Message>(`${this.API}/messages/${message.id}`, message).subscribe(
  //     data => {
  //       this.insertOrUpdate(message)
  //       this.emitMessages();
  //     }
  //   );
  // }


}
