/*
 Transforme les données de firebase
 vers notre format de données à nous
 */
export function transform(data) {
    const messages = [];
    for (const [id, message] of Object.entries(data)) {
      message.id = id;
      messages.push(message);
    }
    return messages;
}

