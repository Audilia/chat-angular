import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  isAuthenticated = false;

  constructor() { }

  login() {
    const promise = new Promise((resolve) => {
        this.isAuthenticated = false;
        resolve();
      });
    return promise;
  }

  logout() {
    this.isAuthenticated = false;
  }

}
