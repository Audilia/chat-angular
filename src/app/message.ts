export interface Message {
  id: string;
  content: string;
  user: any;
  channel: string;
}

