import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { MessageItemComponent } from './message-item/message-item.component';
import { MessagesComponent } from './messages/messages.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule } from '@angular/forms';
import { MessageService } from './message.service';
import { LoginComponent } from './login/login.component';
import { routes } from './routes';
import {AuthService} from './auth.service';
import {AuthGuard} from './auth.guard';
import { NavbarComponent } from './navbar/navbar.component';
import { FriendsComponent } from './friends/friends.component';
import { RoomMenuComponent } from './room-menu/room-menu.component';


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    MessageItemComponent,
    MessagesComponent,
    EditComponent,
    LoginComponent,
    NavbarComponent,
    FriendsComponent,
    RoomMenuComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    MessageService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
