import { Component, OnInit, Input } from '@angular/core';
import {MessageService} from '../message.service';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.scss']
})
export class MessageItemComponent implements OnInit {

  @Input() id: string;
  @Input() content: string;
  @Input() user: any;
  // @Input() index: number;
  // @Input() channel: string;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }

  delete() {
    this.messageService.deleteMessage(this.id);
  }

  // update(message) {
  //   this.messageService.updateMessage(message);
  // }
}
