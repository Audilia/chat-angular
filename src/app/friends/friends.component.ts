import {Component, Input, OnInit} from '@angular/core';
import {MessageService} from '../message.service';
import {User} from '../user';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {

  @Input() id: string;
  @Input() pseudo: string;
  @Input() city: string;
  @Input() birth: Date;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }
}
